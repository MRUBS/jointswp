<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article" itemscope itemtype="http://schema.org/WebPage">
						
	<header class="article-header">
		<h1 class="page-title"><?php the_title(); ?></h1>
	</header> <!-- end article header -->
					
    <section class="entry-content" itemprop="articleBody">
		<!-- start callout -->
		<div class="primary callout">
			<h2>Some Foundation Fun!</h2>
		</div>
		<!-- end callout -->
		
		<!-- start menu -->
		<div class="row">
		  <div class="columns">
			<ul class="dropdown menu" data-dropdown-menu>
			  <li>
				<a href="#Item-1">Item 1</a>
				<ul class="menu">
				  <li><a href="#Item-1A">Item 1A</a></li>
				  <li>
					<a href="#Item-1B">Item 1B</a>
					<ul class="menu">
					  <li><a href="#Item-1Bi">Item 1B i</a></li>
					  <li><a href="#Item-1Bii">Item 1B ii</a></li>
					  <li>
						<a href="#Item-1Biii">Item 1B iii</a>
						<ul class="menu">
						  <li><a href="#Item-1Biiialpha">Item 1B iii alpha</a></li>
						  <li><a href="#Item-1Biiiomega">Item 1B iii omega</a></li>
						</ul>
					  </li>
					  <li>
						<a href="#Item-1Biv">Item 1B iv</a>
						<ul class="menu">
						  <li><a href="#Item-1Bivalpha">Item 1B iv alpha</a></li>
						</ul>
					  </li>
					</ul>
				  </li>
				  <li><a href="#Item-1C">Item 1C</a></li>
				</ul>
			  </li>
			  <li>
				<a href="#Item-2">Item 2</a>
				<ul class="menu">
				  <li><a href="#Item-2A">Item 2A</a></li>
				  <li><a href="#Item-2B">Item 2B</a></li>
				</ul>
			  </li>
			  <li><a href="#Item-3">Item 3</a></li>
			  <li><a href="#Item-4">Item 4</a></li>
			</ul>
		  </div>
		</div>
		<!-- end menu -->
		
		<!-- start modal -->
		<div class="">
			<a href="#" data-reveal-id="myModal">Click Me For A Modal</a>
			<div id="myModal" class="reveal-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
			  <h2 id="modalTitle">Awesome. I have it.</h2>
			  <p class="lead">Your couch.  It is mine.</p>
			  <p>I'm a cool paragraph that lives inside of an even cooler modal. Wins!</p>
			  <a class="close-reveal-modal" aria-label="Close">&#215;</a>
			</div>
		</div>
		<!-- end modal -->
	    <?php the_content(); ?>
	    <?php wp_link_pages(); ?>
	</section> <!-- end article section -->
						
	<footer class="article-footer">
		
	</footer> <!-- end article footer -->
						    
	<?php comments_template(); ?>
					
</article> <!-- end article -->